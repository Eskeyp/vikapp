from .base import *

DEBUG = False

SECRET_KEY = os.environ["SECRET_KEY"]

ALLOWED_HOSTS = ["*"]

os.environ["GOOGLE_APPLICATION_CREDENTIALS"] = os.path.join(
    BASE_DIR, "../vika_app/settings/service.json"
)

DEFAULT_FILE_STORAGE = "vika_app.settings.gcloud.GoogleCloudMediaFileStorage"
STATICFILES_STORAGE = "vika_app.settings.gcloud.GoogleCloudStaticFileStorage"

GS_PROJECT_ID = "ultra-ace-274620"
GS_STATIC_BUCKET_NAME = "vika-app-statics"
GS_MEDIA_BUCKET_NAME = "vika-app-media"

STATIC_URL = "https://storage.googleapis.com/{}/".format(GS_STATIC_BUCKET_NAME)
STATIC_ROOT = "static/"

MEDIA_URL = "https://storage.googleapis.com/{}/".format(GS_MEDIA_BUCKET_NAME)
MEDIA_ROOT = "media/"

UPLOAD_ROOT = "media/uploads/"

DOWNLOAD_ROOT = os.path.join(BASE_DIR, "static/media/downloads")
DOWNLOAD_URL = STATIC_URL + "media/downloads"

STATICFILES_DIRS = (os.path.join(BASE_DIR, "../frontend/static"),)

DATABASES = {
    "default": {
        "ENGINE": "django.db.backends.postgresql_psycopg2",
        "HOST": os.environ["DB_HOST"],
        "PORT": os.environ["DB_PORT"],
        "NAME": os.environ["DB_NAME"],
        "USER": os.environ["DB_USER"],
        "PASSWORD": os.environ["DB_PASSWORD"],
    }
}

FCM_DJANGO_SETTINGS = {
    "FCM_SERVER_KEY": os.environ["FCM_SERVER_KEY"],
}

# DGqCh23ml0nrLPud
