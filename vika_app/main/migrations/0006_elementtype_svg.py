# Generated by Django 2.2.1 on 2020-05-10 10:07

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0005_auto_20200510_1215'),
    ]

    operations = [
        migrations.AddField(
            model_name='elementtype',
            name='svg',
            field=models.ImageField(blank=True, upload_to='images/icons', verbose_name='Иконка svg'),
        ),
    ]
